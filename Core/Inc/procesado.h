/*
 * procesado.h
 *
 *  Created on: 5 nov. 2019
 *      Author: javivi
 */

#include "stm32f0xx_hal.h"
#include "arm_math.h"

#ifndef INC_PROCESADO_H_
#define INC_PROCESADO_H_

#define frameLength 256
#define bufferLength frameLength*2

#endif /* INC_PROCESADO_H_ */
#ifdef __cplusplus
extern "C" {
#endif
#include "stm32f0xx.h"

void procesar(q15_t *inData, q15_t *outData,int32_t length);
void init_procesado(void);



#ifdef __cplusplus
}
#endif
